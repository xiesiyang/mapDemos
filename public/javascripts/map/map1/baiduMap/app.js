//定义map
var map;
//定义地图图层
var	layer;
//定义矢量图层
var vectorLayer;
//点绘制控件
var drawPointControl;
//线绘制控件
var drawLineControl;

var mousePosition;

var modifyFeatureControl;


/**
 * 将经纬度转换为百度地图墨卡托坐标系
 * @param Lonlat(Object{lon:;lat:})
 * @return position(Object{x:;y:;})
 */
function lonLatToPosition(Lonlat) {
	var projection = new BMap.MercatorProjection();
	return projection.lngLatToPoint(new BMap.Point(Lonlat.lon, Lonlat.lat));
}
/**
 * 将百度地图墨卡托坐标系转换为经纬度
 * @param position(object{x:;y:})
 * @return lonlat(object{lng:;lat:})注意百度返回的是lng和lat
 */
function positionToLonLat(position) {
	var projection = new BMap.MercatorProjection();
	return projection.pointToLngLat(new BMap.Pixel(position.x,position.y))
}



function init() {
	map = new SuperMap.Map("map",{projection:new SuperMap.Projection("EPSG:910112"),
        units:"degrees",
		controls: [
		 new SuperMap.Control.LayerSwitcher(),
		 new SuperMap.Control.ScaleLine(),
		 new SuperMap.Control.Zoom(),
		 new SuperMap.Control.Navigation({
			 dragPanOptions: {
				 enableKinetic: true
			 }
		 })
		]
	 });
	$("#map > div").append("<div id='mousePositionDiv' style='height: 30px;position:absolute;right:30px;bottom:0px;z-index:1255'></div>");

	var url ="http://10.206.13.44:8090/iserver/services/map-baidu/rest/maps/normal";
	// var url ="http://localhost:8090/iserver/services/map-ChinaProvinces/rest/maps/ChinaProvinces";
	layer = new SuperMap.Layer.TiledDynamicRESTLayer("world",url,{transparent: true, cacheEnabled: true},{maxResolution:"auto",units:"degree"});
	layer.events.on({"layerInitialized":addLayer});
}




/**
 * 定义矢量图层
 */
function initVectorLayer() {
	vectorLayer = new SuperMap.Layer.Vector("vectorLayer");
}

/**
 * 画线控件中的每一次点击线会调用的方法
 * 这个方法会维护一个要素列表
 * 列表中包含了两个部分,一个管线和一个管线上的管井
 *
 */
function addPoint() {
	console.log(arguments);
}

/**
 * 定义画点,画线,键盘控件
 * 画线过程中要求通过esc取消绘制,delete逐点回退,enter确定绘制功能
 */
function initControls() {
	drawPointControl = new SuperMap.Control.DrawFeature(vectorLayer,SuperMap.Handler.Point,{callbacks:{
		done:function (point) {
			deactiveAll();



		}
	}});

	initDrawLineControl();
	modifyFeatureControl=new SuperMap.Control.ModifyFeature(vectorLayer,{
		"geometryTypes":[SuperMap.Geometry.Point]
	});
	var snap = new SuperMap.Snap([vectorLayer],10,10,{actived:true});

	drawLineControl.snap = snap;
	modifyFeatureControl.snap = snap;
	addData();
	map.addControls([drawPointControl,modifyFeatureControl]);
}

function initDrawLineControl() {
	drawLineControl = new SuperMap.Control.DrawFeature(vectorLayer,SuperMap.Handler.Path,{multi:true,callbacks:{
		point:function () {
		}
	}});

	map.addControls([drawLineControl])
}

/**
 * 处理线绘制过程中的按键操作
 * 要求
 * esc 退出绘制 27
 * delete 逐点回退 46
 * enter 完成绘制 13
 *      由于管井和管沟的关系是两个管井之间的为一条管沟,
 *      而drawLineControl生成的line是一条线上有多个点,模型不符合
 *      所以需要在绘制完成后清空绘制,然后由代码生成管沟和管井.
 * @param e
 */
function lineDrawingKeyBoardHandler(e) {
	switch (e.keyCode){
		case 27:
			drawLineControl.cancel();
			break;
		case 46:
			drawLineControl.undo();
			break;
		case 13:
			drawLineControl.cancel();
			break;
	}
}

function drawPoint() {
	console.log("daf");
	deactiveAll();
	drawPointControl.activate();
}

function drawLine() {
	deactiveAll();
	$("body").on('keyup', lineDrawingKeyBoardHandler);
	drawLineControl.activate();
}
function startModify() {
	deactiveAll();
	modifyFeatureControl.activate();
}

function cancelControl() {
	deactiveAll();
}


function addLayer() {
	initVectorLayer();
	map.addLayers([layer,vectorLayer]);
	initControls();
	var position = lonLatToPosition({lon:102.54,lat:24.35});
	console.log(position);
	map.setCenter(new SuperMap.LonLat(position.x,position.y),12);
	map.events.on({"mousemove":function (e) {
		var position = map.getLonLatFromPixel(e.xy);
		var lonlat = positionToLonLat({x:position.lon,y:position.lat});
		$("#mousePositionDiv")[0].innerText="当前坐标:"+lonlat.lng+","+lonlat.lat;
	}});
	deactiveAll();
}

function deactiveAll(){
	drawPointControl.deactivate();
	drawLineControl.deactivate();
	$("body").off("keyup",lineDrawingKeyBoardHandler);
	modifyFeatureControl.deactivate();
}

function addData() {
	var pointUrl = "../baiduMap/data/points";
	var lineUrl = "../baiduMap/data/lines";
	var pointData = {};
	var lineData = {};
	var time =0;
	$.get(pointUrl,function (pointdata) {
		pointData =pointdata;
		checkData(pointData,lineData);
	});

	$.get(lineUrl,function (linedata) {
		lineData = linedata;
		checkData(pointData,lineData);
	})

	function checkData(pointData,lineData) {
		console.log("第" + (time + 1) + "次调用checkData");
		time++;
		if (pointData && lineData) {
			var point_features=[];
			for(var i= 0,len=pointData.length;i<len;i++){
				var position = lonLatToPosition(pointData[i]);
				var point = new SuperMap.Geometry.Point(position.x,position.x.y);
				var feature=new SuperMap.Feature.Vector(point);
				point_features.push(feature);
			}


			for (var i = 0; i < lineData.length; i++) {
				var line = lineData[i];
				var pointDatas = line.points;
				var points=[];
				for (var j = 0; j < pointDatas.length; j++) {
					var position = lonLatToPosition(pointDatas[j]);
					var point = new SuperMap.Geometry.Point(position.x,position.y);
					points.push(point);
				}
				var line=new SuperMap.Geometry.LineString(points);
				var line_feature=new SuperMap.Feature.Vector(line);
				point_features.push(line_feature);
			}

			vectorLayer.addFeatures(point_features);
		}
	}


	/*
	var point_data=[[-55,34],[-90,-45],[44,-50],[100,33],[94,57]];
	var point_features=[];
	for(var i= 0,len=point_data.length;i<len;i++){
		var point = new SuperMap.Geometry.Point(point_data[i][0],point_data[i][1]);
		var feature=new SuperMap.Feature.Vector(point);
		point_features.push(feature);
	}

	var line_data=[[113,19],[107,-2],[92,13],[90,21],[82,12],[74,3],[64,22],[52,8],[71,0],[91,3]];
	var points=[];
	for(var i= 0,len=line_data.length;i<len;i++){
		var point = new SuperMap.Geometry.Point(line_data[i][0],line_data[i][1]);
		points.push(point);
	}
	var line=new SuperMap.Geometry.LineString(points);
	var line_feature=new SuperMap.Feature.Vector(line);
	point_features.push(line_feature);
*/

}







