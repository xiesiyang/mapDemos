var express = require('express');
var router = express.Router();

router.get("/1",function (req,res,next) {
	res.render("map/map1")
});

router.get("/2",function (req,res,next) {
	res.render("map/map2")
});

router.get("/3",function (req,res,next) {
	res.render("map/map3",{
		url:"http://localhost:3001/tileData/${z}/${x}/${y}.png"
	})
});


module.exports = router;
