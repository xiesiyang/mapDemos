var express = require('express');
var router = express.Router();

var demoDetails=[
	{title:"百度地图服务",url:"/demos/baiduMap",info:"使用SuperMapIserver发布出的百度服务,需要superMapIserver并且superMapIserver要联网"},
	{title:"百度地图离线",url:"/demos/leafletMap",info:"使用百度下载的瓦片图片"},
	{title:"SuperMapBaiduMap",url:"/demos/superMapBaidu",info:"使用superMap封装后的baidu地图api,需要客户端联网"},
	{title:"leaflet百度",url:"/demos/leafletBaidu",info:"使用leaflet-baidu.js"},
	{title:"超图客户端搭配百度地图瓦片",url:"/demos/smbddtwp",info:"超图客户端搭配百度地图瓦片"}
];

var params={
	headTitle:"地图demos",
	bodyTitle:{
		highlight:"地图",
		bold:"demos"
	},
	titleTagLine:"根据几种不同的地图服务来实现的demo",
	intros:[
		"1.由superMap IServer 程序启动服务,superMap iClient for Javascript展示地图和交互操作(IServer程序需付费,启动服务机器需联网)",
		"2.使用superMap iClient for Javascript 中封装的baidu.js展示地图和交互操作(不付费,运行程序的客户机本身需链接外网)",
		"3.使用太乐地图下载器(破解版)下载百度地图离线瓦片图片,由superMap iClient for Javascript 展示地图和交互操作(侵权,百度地图图片不允许下载)"
	],
	items:[
		{
			color:"item-green",
			icon:"icon fa fa-paper-plane",
			title:"SuperMap IServer",
			intro:"",
			link:"/demo/1"
		},
		{
			color:"item-pink",
			icon:"icon icon_puzzle_alt",
			title:"baidu.js",
			intro:"",
			link:"/demo/2"
		},
		{
			color:"item-blue",
			icon:"icon icon_datareport_alt",
			title:"离线地图瓦片",
			intro:"",
			link:"/demo/3"
		}
	]
}




/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', params);
});

module.exports = router;
